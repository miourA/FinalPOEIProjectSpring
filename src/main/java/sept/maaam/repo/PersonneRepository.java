package sept.maaam.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sept.maaam.model.Personne;



public interface PersonneRepository extends JpaRepository<Personne, String>{
	   Personne findByMailAndMdp(String mail, String mdp);
	   //@Query("select p.prenom from Personne p where p.mail = :mail and p.mdp = :mdp")
	   //String testAuth(@Param("mail") String mail, @Param("mdp") String mdp);


}
