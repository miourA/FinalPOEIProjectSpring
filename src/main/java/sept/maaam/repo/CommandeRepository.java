package sept.maaam.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sept.maaam.model.Commande;

public interface CommandeRepository extends JpaRepository<Commande, Integer>{
	@Query("select c from Commande c where c.idUser.id = :mail and c.etatCommande = :status")
	public List<Commande> findByIdUserAndEtatCommande(@Param("mail") String id,@Param("status") String status);
	@Query("select c from Commande c where c.idUser.id = :mail")
	public List<Commande> findCommandebyIdUser(@Param("mail") String mail);
	@Query("select c from Commande c where c.etatCommande = 'en cours' and c.idUser.id = :mail")
	public Commande findongoingcommande(@Param("mail") String mail);
	@Query(nativeQuery = true,value = "select detail_commande from commande where etat_commande = 'en cours' and id_user = :mail LIMIT 1")
	public String getlastcommandetail(@Param("mail") String mail);
}
