package sept.maaam.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import sept.maaam.model.Article;



public interface ArticleRepository extends JpaRepository<Article, Integer>{
	public List<Article> findByDescriptionContainingOrMarqueContaining(String chaine,String str);
	public List<Article> findByPrixBetween(double min, double max);
	public List<Article> findAllByOrderByPrixAsc();
	public List<Article> findAllByOrderByPrixDesc();
	public List<Article> findByPrixLessThan(double valeur);
	public Optional<Article> findFirstByMarque(String marque);
	public int countByMarque(String marque);
	public List<Article> findAllByOrderByMarqueAsc();
	public List<Article> findAllByOrderByIdAsc();
	public List<Article> findByMarque(String marque);

}
