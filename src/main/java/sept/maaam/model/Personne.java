package sept.maaam.model;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Personne {
	private String mail;
	private String mdp;
	private String nom;
	private String prenom;
	private String telephone;
	private String adresse;
	private String role;
	private Collection<Commande> commande;
	private int version;

	public Personne(String mail, String mdp, String nom, String prenom, String role) {
		this.mail = mail;
		this.mdp = mdp;
		this.nom = nom;
		this.prenom = prenom;
		this.telephone = telephone;
		this.adresse = adresse;
		this.role = role;
	}

	public Personne() {
	}

	public String getTelephone() {
		return telephone;
	}

	@Id
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@OneToMany(mappedBy = "idUser", fetch = FetchType.LAZY)
	@JsonIgnore
	public Collection<Commande> getCommande() {
		return commande;
	}

	public void setCommande(Collection<Commande> commande) {
		this.commande = commande;
	}

	@Version
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Personne [mail=" + mail + ", mdp=" + mdp + ", nom=" + nom + ", prenom=" + prenom + ", telephone="
				+ telephone + ", adresse=" + adresse + ", role=" + role + "]";
	}

}
