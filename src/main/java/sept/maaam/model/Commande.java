package sept.maaam.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;

@Entity
public class Commande {
	private int id;
	private String detailCommande; // idart1-qt1;idart2-qt2;...
	private String etatCommande;
	private Personne idUser;
	private int version;

	public Commande() {
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "command_jpa_sequence")
	@SequenceGenerator(name = "command_jpa_sequence", sequenceName = "command_id_sequence")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "idUser", referencedColumnName = "mail")
	public Personne getIdUser() {
		return idUser;
	}

	public void setIdUser(Personne idUser) {
		this.idUser = idUser;
	}

	public String getEtatCommande() {
		return etatCommande;
	}

	public void setEtatCommande(String etatCommande) {
		this.etatCommande = etatCommande;
	}

	public String getDetailCommande() {
		return detailCommande;
	}

	public void setDetailCommande(String detailCommande) {
		this.detailCommande = detailCommande;
	}

	@Version
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

}
