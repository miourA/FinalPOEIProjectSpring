package sept.maaam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sept.maaam.model.Article;
import sept.maaam.repo.ArticleRepository;



@RestController
@RequestMapping("/articlerest")
public class ArticleRestController {

	@Autowired
	private ArticleRepository repo;
	
	@CrossOrigin
	@GetMapping("/prixasc")
	public List<Article> prixasc(){
		return repo.findAllByOrderByPrixAsc();
	}
	
	@CrossOrigin
	@GetMapping("/prixdesc")
	public List<Article> prixdesc(){
		return repo.findAllByOrderByPrixDesc();
	}

	@CrossOrigin
	@GetMapping("/countbymarque/{marque}")
	public int m10(@PathVariable String marque) {

		return repo.countByMarque(marque);
	}

	@CrossOrigin
	@GetMapping("/findbydesc/{chaine}")
	public List<Article> m9(@PathVariable String chaine) {

		return repo.findByDescriptionContainingOrMarqueContaining(chaine,chaine);
	}

	@CrossOrigin
	@GetMapping("/findbyprix/{min}/{max}")
	public List<Article> m8(@PathVariable int min, @PathVariable int max) {

		return repo.findByPrixBetween(min, max);
	}

	@CrossOrigin
	@GetMapping("/findbymarque/{marque}")
	public List<Article> m7(@PathVariable String marque) {

		return repo.findByMarque(marque);
	}

	@CrossOrigin
	// La casse est ignoree (repo)
	@GetMapping("/findallmarque")
	public List<Article> m6() {
		return repo.findAllByOrderByMarqueAsc();
	}

	// Pour le test sur postman, d'abord faire un findbyid/id en GET,
	// Puis r�cup�rer la Article et la copier dans body sans toucher � la
	// version
	// taper update en PUT et voir la modif en base

	@CrossOrigin
	@PutMapping
	public void update(@RequestBody Article a) {
		 this.repo.save(a);
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public void deletebyid(@PathVariable int id) {
		repo.deleteById(id);
	}

	// @CrossOrigin
	// @PostMapping
	// public void create(@RequestBody Article a)
	// {
	// repo.save(a);
	// }
	@PostMapping
	@CrossOrigin
	public ResponseEntity<Article> insert(@RequestBody Article a) {
		repo.save(a);
		return new ResponseEntity<Article>(repo.findById(a.getId()).get(), HttpStatus.CREATED);
	}

	@CrossOrigin
	@GetMapping("{id}")
	public Article findbyid(@PathVariable int id) {

		return repo.findById(id).get();
	}

	@CrossOrigin
	@GetMapping
	public List<Article> findall() {

		return repo.findAll();
	}

	@CrossOrigin
	@GetMapping("/hello")
	public String getHello() {
		return "Hello World!";
	}
}
