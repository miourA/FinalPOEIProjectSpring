package sept.maaam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sept.maaam.model.Commande;
import sept.maaam.repo.CommandeRepository;

@RestController
@RequestMapping("commandRest")
public class CommandeRestController {
	@Autowired
	CommandeRepository repo;

	@CrossOrigin
	@GetMapping
	public List<Commande> findall() {
		return repo.findAll();
	}

	@CrossOrigin
	@GetMapping("{id}")
	public Commande findById(@PathVariable int id) {
		return repo.findById(id).get();
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Object> create(@RequestBody Commande c) {

		repo.save(c);
		return new ResponseEntity<Object>(HttpStatus.CREATED);

	}

	@CrossOrigin
	@PutMapping
	public ResponseEntity<Object> update(@RequestBody Commande c) {
		repo.save(c);
		return new ResponseEntity<Object>(HttpStatus.ACCEPTED);
	}

	@CrossOrigin
	@DeleteMapping("{id}")
	public ResponseEntity<Object> delete(@PathVariable int id) {
		repo.deleteById(id);
		return new ResponseEntity<Object>(HttpStatus.ACCEPTED);
	}
	
	@CrossOrigin
	@GetMapping("getcommande/{id}/{status}")
	public List<Commande> getcommande(@PathVariable String id,@PathVariable String status){
		return repo.findByIdUserAndEtatCommande(id, status);
	}
	
	@CrossOrigin
	@GetMapping("getcommande/{id}")
	public Commande getongoingcommande(@PathVariable String id){
		return repo.findongoingcommande(id);
	}
	
	@CrossOrigin
	@GetMapping("getUserLastCartString/{id}")
	public String getUserCommandeString(@PathVariable String id){
		return repo.getlastcommandetail(id);
	}
	
	@CrossOrigin
	@GetMapping("getCoByUserId/{id}")
	public List<Commande> getCoByUserId(@PathVariable String id){
		return repo.findCommandebyIdUser(id);
	}

}
