package sept.maaam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import sept.maaam.model.Article;
import sept.maaam.model.Personne;
import sept.maaam.repo.PersonneRepository;



@RestController
@RequestMapping("/personne")
public class PersonneRestController {
	@Autowired
	private PersonneRepository prepo;

	@CrossOrigin
	@DeleteMapping("{mail}")
	public void deletebyid(@PathVariable String mail) {
		prepo.deleteById(mail);
	}
	
	@CrossOrigin
	@PutMapping
	public void update(@RequestBody Personne p) {
		 this.prepo.save(p);
	}
	
	@CrossOrigin
	@GetMapping("{mail}")
	public Personne findbyid(@PathVariable String mail) {

		return prepo.findById(mail).get();
	}
	
	@CrossOrigin
	@GetMapping
	public List<Personne> list() {
		return this.prepo.findAll();
	}

	@CrossOrigin
	@PostMapping
	public ResponseEntity<Object> create(@RequestBody Personne personne) {
		if (!this.prepo.existsById(personne.getMail())) {
			this.prepo.save(personne);
			return new ResponseEntity<Object>(null, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<Object>(null, HttpStatus.BAD_REQUEST);
		}
	}

	@CrossOrigin
	@GetMapping("/{mail}/{mdp}")
	public Personne findByMailAndMdp(@PathVariable(name = "mail") String mail,
			@PathVariable(name = "mdp") String mdp) {
		Personne p = prepo.findByMailAndMdp(mail, mdp);
		p.setMdp("DO_NOT_TRANSFER_THIS_DATA");
		return p;

	}

}
