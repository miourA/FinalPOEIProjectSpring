package sept.maaam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaaamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaaamApplication.class, args);
	}

}
