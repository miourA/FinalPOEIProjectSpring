package sept.maaam.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import sept.maaam.model.Article;
import sept.maaam.model.Personne;
import sept.maaam.repo.ArticleRepository;
import sept.maaam.repo.PersonneRepository;



@Service
public class ConsoleService implements CommandLineRunner {
	@Autowired
	//ArticleRepository arepo;
	PersonneRepository prepo;

	@Override
	public void run(String... args) throws Exception {
		
//		System.out.println("Liste des Articles");
//		for (Article a: arepo.findAll())
//		{
//			System.out.println(a);
//		}
		
		System.out.println("Liste des Personnes");
		for (Personne p: prepo.findAll())
		{
			System.out.println(p);
		}

	}

}
